from django.db import models

# Create your models here.
class Review(models.Model):
    cover_image = models.URLField()
    title = models.CharField(max_length=100)
    rating = models.SmallIntegerField()
    authors = models.CharField(max_length=100)
    summary = models.TextField()
    review = models.TextField()
    created = models.DateTimeField()


#class is Review and the properties are cover_image,
#title, rating, authors, etc.

# model class Review talks to the database, so need 
# to update the database to contain reviews and all
# those properties like title, rating, etc. 